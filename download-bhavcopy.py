"""Downloads all Bhavcopy reports from 2007 till date as a CSV.

Usage:
    download-bhavcopy.py
"""
from __future__ import print_function

import os
import os.path
import httplib2
import zipfile
from zipfile import BadZipfile
import StringIO
import datetime


class BSE():

    def __init__(self):
        self.browser = httplib2.Http('.cache')

    def bhavcopy(self, date):
        """Fuction to call url and fetch all csv files."""
        url = 'http://www.bseindia.com/download/BhavCopy/Equity/EQ{:%d%m%y}_CSV.ZIP'.format(date)
        resp, content = self.browser.request(url)

        # If all is OK, download the zip file and extract it
        if resp.status == 200:
            stream = StringIO.StringIO(content)
            try:
                archive = zipfile.ZipFile(stream)
            except BadZipfile:
                return None
            for filename in archive.namelist():
                return archive.open(filename).read()

        # If it's forbidden, then no Bhavcopy exists for that date.
        # Explicitly return an empty string
        elif resp.status == 403:
            return ''

        # Any other status is a potential error. Just give up
        else:
            return None


def daterange(start_date, end_date):
    """Range of date."""
    for n in range((end_date - start_date).days):
        yield start_date + datetime.timedelta(n)


bse = BSE()
path = os.path.normpath(os.path.join(os.path.dirname(__file__), 'bhavcopy'))
if not os.path.exists(path):
    os.makedirs(path)

start_date = datetime.date(2013, 3, 19)
end_date = datetime.date.today()

for date in daterange(start_date, end_date):
    # Ignore Saturdays & Sundays
    if 5 <= date.weekday() <= 6:
        continue

    filename = os.path.join(path, date.strftime('%Y-%m-%d.csv'))
    if os.path.exists(filename):
        continue

    bhavcopy = bse.bhavcopy(date)
    if bhavcopy is not None:
        with open(filename, 'w') as stream:
            stream.write(bhavcopy)
        print(filename, 'OK')
    else:
        print(filename, 'FAIL')
